package com.example.viewdemo;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements OnTouchListener{
	private MyView view;
	private String TAG=this.getClass().getSimpleName();
	private LinearLayout.LayoutParams params;
	private Rect outRect = new Rect();
	private WindowManager.LayoutParams wParams;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		view=(MyView) findViewById(R.id.my_view);
		view.setOnTouchListener(this);
		params=(LinearLayout.LayoutParams)view.getLayoutParams();
	}
	private int xDown;
	private int yDown;
	private int initLeft;
	private int initTop;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MoveTask moveTask=new MoveTask();
		moveTask.execute();
		addSystemView();
	}
	private void addSystemView(){
		WindowManager manager=(WindowManager)getApplicationContext().getSystemService(Context.WINDOW_SERVICE);;
		TextView tv=new TextView(this);
		tv.setText("Test");
		setParams();
		manager.addView(tv, wParams);
	}
	private void setParams(){
		wParams=new WindowManager.LayoutParams();
		wParams.type=WindowManager.LayoutParams.TYPE_SYSTEM_ALERT; 
		wParams.format=1;  
		wParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE; // 不能抢占聚焦点  
		wParams.flags = wParams.flags | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;  
		wParams.flags = wParams.flags | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS; // 排版不受限制  

		wParams.alpha = 1.0f;  

		wParams.gravity=Gravity.CENTER;   //调整悬浮窗口至左上角  
		//以屏幕左上角为原点，设置x、y初始值  
		wParams.x=0;  
		wParams.y=0;  

		//设置悬浮窗口长宽数据  
		wParams.width=140;  
		wParams.height=140;  
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		Log.i(TAG, "view width:"+view.getWidth());
		Log.i(TAG, "view height:"+view.getHeight());
		Log.i(TAG,"getX:"+event.getX());
		Log.i(TAG,"getRawX:"+event.getRawX());
		Log.i(TAG,"getLeft:"+view.getLeft());
		Log.i(TAG,"getTop:"+view.getTop());
		Log.i(TAG,"getRight:"+view.getRight());
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			initLeft=view.getLeft();
			initTop=view.getTop();
			xDown=(int) event.getRawX();
			yDown=(int)event.getRawY();
			flag=false;
			break;
		case MotionEvent.ACTION_MOVE:
			int dx=(int)(event.getRawX()-xDown);
			int dy=(int)(event.getRawY()-yDown);
			view.layout(initLeft+dx, initTop+dy, initLeft+dx+view.getWidth(), initTop+dy+view.getHeight());
			break;
		case MotionEvent.ACTION_UP:
			flag=true;
			leftMargin=view.getLeft();
			topMargin=view.getTop();
			MoveTask moveTask=new MoveTask();
			moveTask.execute();
			break;
		}
		return true;
	}
	private void sleep(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private int topMargin=0;
	private int leftMargin=0;
	private boolean flag=true;
	private class MoveTask extends AsyncTask<Integer,Integer,Integer>{
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			boolean flag1=true;
			boolean flag2=true;
			while(flag){
				sleep(20);
				if(flag1){
					topMargin+=3;
				}else{
					topMargin-=3;
				}
				if(flag2){
					leftMargin+=3;
				}else{
					leftMargin-=3;
				}
				if(topMargin>outRect.height()-view.getHeight()){
					flag1=false;
				}
				if(topMargin<0){
					flag1=true;
				}
				if(leftMargin>outRect.width()-view.getWidth()){
					flag2=false;
				}
				if(leftMargin<0){
					flag2=true;
				}
				publishProgress();
			}
			return 0;
		}
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			params.topMargin=topMargin;
			params.leftMargin=leftMargin;
			view.setLayoutParams(params);
		}

	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){
			this.getWindow().findViewById(Window.ID_ANDROID_CONTENT).getDrawingRect(outRect);
		}
	}

}
