package com.example.viewdemo;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

public class MyView extends LinearLayout{
	private String TAG=this.getClass().getSimpleName();
	public MyView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onMeasure");
		if(MeasureSpec.getMode(widthMeasureSpec)==MeasureSpec.AT_MOST){
			Log.i(TAG, "width mode:AT_MOST");
		}else if(MeasureSpec.getMode(widthMeasureSpec)==MeasureSpec.EXACTLY){
			Log.i(TAG, "width mode:EXACTLY");
		}else if(MeasureSpec.getMode(widthMeasureSpec)==MeasureSpec.UNSPECIFIED){
			Log.i(TAG, "width mode:UNSPECIFIED");
		}
		if(MeasureSpec.getMode(heightMeasureSpec)==MeasureSpec.AT_MOST){
			Log.i(TAG, "height mode:AT_MOST");
		}else if(MeasureSpec.getMode(heightMeasureSpec)==MeasureSpec.EXACTLY){
			Log.i(TAG, "height mode:EXACTLY");
		}else if(MeasureSpec.getMode(heightMeasureSpec)==MeasureSpec.UNSPECIFIED){
			Log.i(TAG, "height mode:UNSPECIFIED");
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onLayout changed:"+changed);
		Log.i(TAG, "onLayout l:"+l);
		Log.i(TAG, "onLayout t:"+t);
		Log.i(TAG, "onLayout r:"+r);
		Log.i(TAG, "onLayout b:"+b);
		super.onLayout(changed, l, t, r, b);
	}

}
